import json
from datetime import datetime
import boto3, random
from botocore.exceptions import ClientError
import os

dynamodb = boto3.client('dynamodb')


def _send_to_connection(connection_id, data, event):
    gatewayapi = boto3.client("apigatewaymanagementapi",
            endpoint_url = "https://" + event["requestContext"]["domainName"] +
                    "/" + event["requestContext"]["stage"])
    return gatewayapi.post_to_connection(ConnectionId=connection_id,
            Data=json.dumps(data).encode('utf-8'))

def _get_response(status_code, body):
    if not isinstance(body, str):
        body = json.dumps(body)
    return {"statusCode": status_code, "body": body}


def get_user_messages(event, context):
    """
    Return the 10 most recent chat messages.
    """
    # logger.info("Retrieving most recent messages.")
    connectionID = event["requestContext"].get("connectionId")
    partner_id = json.loads(event['body']).get('partner_id',None)

    # partner_id = event['queryStringParameters']['partner_id']

    data = dynamodb.scan(
    TableName=os.environ['SOCKET_MESSAGE_TABLE_NAME'],
    FilterExpression="reciever = :name",
    ExpressionAttributeValues={":name": {'S':partner_id}}
    )

    # Extract the relevant data and order chronologically
    messages = [{"id": str(x["id"]).split(":")[1].replace("}", ""),
    "user":str(x["user"]).split(":")[1].replace("}", ""), 
    "message": str(x["message"]).split(":")[1].replace("}", ""), 
    "reciever": str(x["reciever"]).split(":")[1].replace("}", ""),
    "imageSrc": str(x["imageSrc"]).split(":")[1].replace("}", ""),
    "status": str(x["status"]).split(":")[1].replace("}", "")}
            for x in data.get('Items')]
    messages.reverse()

    data = {"messages": messages}
    _send_to_connection(connectionID, data, event)

    return _get_response(200, "Sent recent messages.")

def get_active_users(event, context):
    connectionID = event["requestContext"].get("connectionId")
    data = dynamodb.scan(
    TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME']
    )

    users = [{"username": str(x["username"]).split(":")[1].replace("}", "")}
            for x in data.get('Items')]
    users.reverse()

    data = {"users": users}
    _send_to_connection(connectionID, data, event)

    return _get_response(200, "Sent active users.")

