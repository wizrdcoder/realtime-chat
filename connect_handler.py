import json
import boto3
import os

dynamodb = boto3.client('dynamodb')

def handler(event, context):
    connectionId = event['requestContext']['connectionId']
    username = event['queryStringParameters']['username']

    dynamodb.put_item(TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME'], Item={'connectionId':{'S': connectionId}, 
        'username':{'S': username}, 'status':{'S': 'online'}})

    # fetch previous messages 
    data = dynamodb.scan(
    TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME']
  )
    connectionIds = []

    apigatewaymanagementapi = boto3.client('apigatewaymanagementapi', 
    endpoint_url = "https://" + event["requestContext"]["domainName"] + "/" + event["requestContext"]["stage"])
    
    for connectionId in connectionIds:
        if connectionId['username']['S'] == username:
            apigatewaymanagementapi.post_to_connection(
                Data=json.dumps(data),
                ConnectionId=connectionId['connectionId']['S']
            )
        

    response = {
        'statusCode': 200,
        'body': json.dumps(data),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

    return response