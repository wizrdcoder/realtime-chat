import json
from datetime import datetime
import boto3, random
import os

dynamodb = boto3.client('dynamodb')
# connectionId = event['requestContext']['connectionId']

def handler(event, context):
    message = json.loads(event['body']).get('message',None)
    reciever_username = json.loads(event['body']).get('partner_id',None)
    user = json.loads(event['body']).get('user', None)

    curr_dt = datetime.now()
    id = str(int(round(curr_dt.timestamp())) + random.randint(0,1000))
    now = datetime.now()
    now = now.strftime('%Y-%d-%m %H:%M:%a')

    dynamodb.put_item(TableName=os.environ['SOCKET_MESSAGE_TABLE_NAME'], Item={'user':{'S': user}, 'id':{'N': id}, 'message':{'S': message},
    'reciever':{'S': reciever_username}, 'imageSrc':{'S':''}, 'status':{'S':'Online'}, 
    'timestamp':{'S':now}})
    
    paginator = dynamodb.get_paginator('scan')
    
    connectionIds = []

    apigatewaymanagementapi = boto3.client('apigatewaymanagementapi', 
    endpoint_url = "https://" + event["requestContext"]["domainName"] + "/" + event["requestContext"]["stage"])
    
    for page in paginator.paginate(TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME']):
        connectionIds.extend(page['Items'])

    if not reciever_username:
        for connectionId in connectionIds:
            apigatewaymanagementapi.post_to_connection(
                Data=message,
                ConnectionId=connectionId['connectionId']['S']
            )
    else:
        # Emit the recieved message to reciever connected devices
        for connectionId in connectionIds:
            print(connectionId['username'])
            if connectionId['username']['S'] == reciever_username:
                apigatewaymanagementapi.post_to_connection(
                    Data=message,
                    ConnectionId=connectionId['connectionId']['S']
                )
        
    return {}
 