import json
import boto3
import os

dynamodb = boto3.client('dynamodb')

def handler(event, context):
    connectionId = event['requestContext']['connectionId']
    dynamodb.delete_item(TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME'], Key={'connectionId':{'S': connectionId}})

    return { 
            "isBase64Encoded":False,
            "statusCode": 200, 
            "headers":{"status":"success"},
            "body":"disconnected"
        }